%include 'colon.inc'

colon "hello", hello
db "Never burp in people's faces when you meet them.", 10, 0

colon "world", world
db "Don't litter in the streets. We're already killing the our planet.", 10, 0

colon "good", good
db "Guys, let's live together!", 10, 0

colon "death", death
db "Death is the only inevitable thing in your life.", 10, 0
