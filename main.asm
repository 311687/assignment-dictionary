%define buff 256

section .text

extern read_word
extern find_word
extern print_newline
extern print_string
extern print_error
extern string_length
extern exit

global _start

section .rodata
msg_noword: db "No such word",0

%include "words.inc"

section .text
_start:
push rbp
mov rbp, rsp
sub rsp, buff
mov rdi, rsp
call read_word
test rax, rax
jz .bad
mov rdi, rax
mov rsi, next
call find_word
test rax, rax
jz .bad

add rax, 8
push rax
mov rax, [rsp]
mov rdi, rax
call string_length
pop rdi
add rdi, rax
inc rdi
call print_string
mov rsp, rbp
pop rbp
call exit

.bad:
mov rdi, msg_noword
call print_error

mov rsp, rbp
pop rbp
call exit
